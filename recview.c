#include <stdlib.h>
#include <stdio.h>
 
int c, i, recordingday, mmsb, msb, lsb, rec_time_ms, hours, minutes, seconds,
   milliseconds, space;
int errorcode=0, linenr=0, padding=165, error=0, emptyfile=0, reclen=0;
FILE *f;
unsigned char *y;

int getreclen() {
  c = fgetc(f);
  if (c != -1 ) {
    reclen=c*256;
    c = fgetc(f);
    reclen= c + reclen;
    /* emptyfile will remain 0 only if the file is totally empty */
    if ( emptyfile == 0 ) {
      emptyfile = 1;
    }
  } 
  else {
    reclen = -1;
  }
  return reclen;
}

int geterrorcode() {
  c = fgetc(f);
  return c;
}

int getlinenr() {
  c = fgetc(f);
  return c;
}

/* recordingday is not printed by recview*/
int getrecordingday() {
  c = fgetc(f);
  return c;
}

int getmmsb() {
  c = fgetc(f);
  return c;
}

int getmsb() {
  c = fgetc (f);
  return c;
}

int getlsb() {
  c = fgetc (f);
  return c;
}

void hexdump(int reclen) {
  for (i=0; i<reclen-12; i++)
  {
    c = fgetc (f);
    if (c != -1 ) {
      y[i] = c;
    }
    else {
      reclen = -1;
    }
  }
}

void printdump(unsigned char *y) {
  space=0;
  /* We toggle a white space after each 2 iterations */
  for (i=0; i<reclen-12; i++)
    {
    if (space == 0) {
      /* No white space after y[i] here */
      printf ("%02X", y[i]);
      space = 1;
    }
    else
    {
      /* White space after y[i] here */
      printf ("%02X ", y[i]);
      space = 0;
    }
  }
  printf ("\n");
}

int getpadding() {
  for (i = 0; i < 4 ; i++) {
    c = fgetc(f);
    if (c != 165) {
      padding = 0;
    }
  }
  return padding;
}

int main (int argc, char *argv[])
{

  if (argc != 2) {
    fprintf (stderr,"usage: recview <file>\n");
    fprintf (stderr,"<file> input file in Final Format\n");
    return 1;
  }

  f = fopen(argv[1], "r");
  if (f == 0) {
    fprintf (stderr,"error. unable to open file %s\n", argv[1]);
    return 1;
  }

  while ( reclen != -1 ) {
    reclen=getreclen();
    if (emptyfile == 0 ) {
      /* if we are given an empty file emptyfile = 1
         yet reclen is still 0, so this seems the best way to deal with this
         exception, otherwise recview would not return error message and exit
         code */
      fprintf (stderr,"error: wrong file format.\n");
      error = 1;
    }
    if (reclen != -1 ) {
      y = realloc (y, sizeof(int) * (reclen - 11));
      if (y == 0) {
        perror ("malloc failed");
        abort ();
      }
      errorcode = geterrorcode();
      linenr = getlinenr();
      recordingday = getrecordingday();
      mmsb = getmmsb();
      msb = getmsb();
      lsb = getlsb();
      hexdump(reclen);
      padding = getpadding();
      if (reclen == -1 || errorcode == -1 || linenr == -1 || recordingday == -1
        || mmsb == -1 || msb == -1 || lsb == -1 || padding == 0) {
        reclen = -1;
        error = 1;
        if (emptyfile == 1) {
          fprintf (stderr,"error: wrong file format.\n");
          }
        else
          {
          /* emptyfile = 2, so there has been good records, but now we have problems */
          fprintf (stderr,"error: invalid record, file may corrupted.\n");
          }
        break;
      }
      /* Everything is in order so we compute the time and proceed with the printing */
      rec_time_ms = mmsb * 65536 + msb * 256 + lsb;
      hours = rec_time_ms / 360000;
      minutes = (rec_time_ms % 360000) /6000;
      seconds =(rec_time_ms/100) % 60;
      milliseconds = rec_time_ms % 100;
      printf ("\nL%02X E%02X T%02d:%02d:%02d,%02d ->  ",
        linenr, errorcode, hours, minutes, seconds, milliseconds);
      printdump(y);
      /* At least one record was printed so emptyfile now has a value of 2 */
      emptyfile = 2;
    }
  }
  fclose(f);
  free (y);
  return error;
}
